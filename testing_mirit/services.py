# -*- coding: utf-8 -*-
from datetime import datetime

from prettytable import PrettyTable

from commit import CommitUser
from user import User


def get_commits(file):
    """
    Метод для считывания данных из файла построчно
    :param file: файл из которого считываются данные
    :return: возварщает список CommitUser и дату последнего коммита
    """
    result = []
    last_date_commit = ''
    for raw in file:
        commit = CommitUser()
        if 'committer' in raw:
            commit.commiter = raw.split(':')[1].split('\n')[0]
            result.append(commit)
        elif 'timestamp' in raw:
            date_string = raw.split('p:')[1].split('+')[0].strip()
            if not last_date_commit:
                last_date_commit = date_string
            check_list_commits(result, date_string)
    return result, last_date_commit


def get_list_users(list_commits):
    """
    Метод для заполения списка пользователей
    :param list_commits: массив коммитов
    :return: массив пользователей с именем и датами коммитов
    """
    list_user = []
    list_user_name = []
    for commit in list_commits:
        if commit.commiter not in list_user_name:
            new_user = User(commit.commiter, commit.date)
            list_user_name.append(commit.commiter)
            list_user.append(new_user)
        for user in list_user:
            if commit.commiter == user.name and user.list_date and commit.date not in user.list_date:
                user.list_date.append(commit.date)
    return list_user


def check_list_commits(list_commits, date):
    """
    Метод для проверки и заполения дат у пользователя
    :param list_commits: массив коммитов
    :param date: проверяемая дата
    """
    for commit in list_commits:
        if not commit.date:
            commit.date = date


def get_list_years(list_commits):
    """
    Метод возвращает года
    :param list_commits: массив коммитов
    :return: массив годов
    """
    list_years = []
    for commit in list_commits:
        date_commit = string_to_date(commit.date)
        if date_commit.year not in list_years:
            list_years.append(date_commit.year)
    return list_years


def get_table(list_users_calc,list_years):
    """
    Метод возварщает заполненнуб таблицу
    :param list_users_calc: массив пользователей
    :param list_years: массив годов
    :return: таблица
    """
    list_header = ['Пользователь', '3 месяца', '% 3 месяца']
    for year in list_years:
        list_header.append(str(year))
        list_header.append('% ' + str(year))
    list_header.append('Общее количество ')
    list_header.append('Общий % ')

    table = PrettyTable(list_header)

    for user in list_users_calc:
        raw_res = [user['name'], user['count_3_month'], user['proc_mounth']]
        for year in list_years:
            raw_res.append(user[year])
            raw_res.append(user['proc_year_' + str(year)])
        raw_res.append(user['count'])
        raw_res.append(user['proc_com'])
        table.add_row(raw_res)
    return table

def string_to_date(date):
    """
    Метод для перевода строки в дату
    :param date: строка даты
    :return: дата с типом datetime
    """
    return datetime.strptime(date, "%a %Y-%m-%d %H:%M:%S")


def calc_commit(list_users, list_years, last_date_commit):
    """
    Метод для подсчета коммитов
    :param list_users: массив пользователей
    :param list_years: массив годов
    :param last_date_commit: дата последнего коммита
    :return: массив пользователей с рассчетанными коммитами
    """
    result = []
    for user in list_users:
        count_3_month = 0
        count = 0
        res = {
            'name': user.name
        }
        for year in list_years:
            count_year = 0
            for date in user.list_date:
                count += 1
                count_month = 3
                count_day = 28 * 3
                now_date = string_to_date(last_date_commit)
                date_d = string_to_date(date)
                m_date = now_date - date_d
                if m_date.days < count_day:
                    count_3_month += 1
                if year == date_d.year:
                    count_year += 1
            res[year] = count_year
        res['count'] = count / len(list_years)
        res['count_3_month'] = count_3_month / len(list_years)

    result.append(res)
    return result

