# -*- coding: utf-8 -*-
import sys
from services import *


if len(sys.argv) == 2:
    PATH_FILE = sys.argv
else:
    print("Введите путь до файла: ")
    PATH_FILE = raw_input()

with open(PATH_FILE, "r") as opening:
    file_commiter = opening.readlines()

list_commits, last_date_commit = get_commits(file_commiter)
list_users = get_list_users(list_commits)
list_years = sorted(get_list_years(list_commits))
list_users_calc = calc_commit(list_users, list_years, last_date_commit)
count_com_year = {}
count_commit = 0
count_mounth = 0

for user in list_users_calc:
    count_commit += user['count']
    count_mounth += user['count_3_month']
    for year in list_years:
        if count_com_year.has_key(year):
            count_com_year[year] += user.get(year)
        else:
            count_com_year[year] = user.get(year)

for user in list_users_calc:
    proc_mounth = user['count_3_month'] * 100.0 / count_mounth
    proc_com = user['count'] * 100.0 / count_commit

    for year in list_years:
        proc_year = user[year] * 100.0 / count_com_year[year]
        user['proc_year_' + str(year)] = round(proc_year, 2)
    user['proc_mounth'] = round(proc_mounth, 2)
    user['proc_com'] = round(proc_com, 2)

table = get_table(list_users_calc, list_years)

print table
